import unittest
import datetime
import os
import sys


class TestPackagesExist(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.folder = os.path.dirname(__file__)

    def setUp(self):
        print("testing is running...")

    def test_data_package(self):
        import bymvisualstudio
        from bymvisualstudio.Actions.Command import Command
        from bymvisualstudio.Actions.Query import Query
        self.assertEqual(Command().createAction(), "create")
        self.assertEqual(Query().getSingle(), "getsingle")

    def test_scripts_package(self):
        import bymvisualstudio
        from bymvisualstudio.Scripts.setup_actions import setupActions


if __name__ == "__main__":
    unittest.main()
