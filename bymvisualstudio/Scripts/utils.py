import os
import re
import shutil
from colored import fg, bg, attr


class Utils:

    def __init__(self):
        print("\n")

    def printRed(self, text):
        print('%s %s %s' % (fg(1), text, attr(0)))

    def printGreen(self, text):
        print('%s %s %s' % (fg(22), text, attr(0)))

    def isEqual(self, str1, str2):
        return str1.lower() == str2.lower()

    def checkServiceNameAndRootDirname(self, servicename, rootdir):
        dirname = rootdir.lower()
        scrap = ""
        if "integrasjontest" in dirname:
            scrap = dirname.replace("integrasjontest", "")
        elif "test" in dirname:
            scrap = dirname.replace("test", "")
        return self.isEqual(scrap, servicename)

    def replaceCaseSensitiv(self, inputText, strOld, strNew):
        return inputText.replace(strOld, strNew)

    def replaceCaseIgnore(self, inputText, strOld, strNew):
        source_str = re.compile(strOld, re.IGNORECASE)
        replaced = source_str.sub(strNew, inputText)
        return replaced

    def rootDirectory(self):
        currentDir = os.getcwd()
        # return os.path.join(currentDir, self.packageName)
        return currentDir

    def rootDirectoryOfModule(self, modul):
        return os.path.dirname(modul.__file__)

    def currentLocation(self):
        packageRootPath = os.path.dirname(os.path.realpath(__file__))
        # return os.path.join(packageRootPath, self.packageName)
        return packageRootPath

    def fullPath(self, filename):
        currentDir = self.rootDirectory()
        filepath = os.path.join(currentDir, filename)
        return filepath

    def joinpath(self, dirname, subdirname, filename):
        currentDir = self.rootDirectory()
        filepath = os.path.join(
            currentDir, subdirname + "\\", dirname + "\\", filename)
        print(filepath)
        return filepath

    def CopyFile(self, dirsource, dirtarget, filename):
        sourcePath = os.path.join(dirsource, filename)
        targetPath = os.path.join(dirtarget, filename)
        shutil.copy(sourcePath, targetPath)

    def walkDir(self, path, dirOptions=[]):
        list_of_files = {}
        for dir in os.listdir(path):
            if dir in dirOptions:
                dirpath = os.path.join(path, dir)
                arr = []
                for filename in os.listdir(dirpath):
                    # if filename.endswith(".py"):
                    filepath = os.path.join(dirpath, filename)
                    if "__pycache__" not in filename:
                        arr.append(filepath)
                list_of_files[dir] = arr
        return list_of_files

    def makeDir(self, dir):
        dirpath = self.fullPath(dir)
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)
        return dirpath

    def makeFile(self, file, text):
        with open(file, "a") as fw:
            fw.write(text)
            print("Created new file: {}".format(file))

    def writeToFile(self, dir, filename, text):
        dirpath = self.makeDir(dir)
        file = os.path.join(dirpath, filename)
        self.makeFile(file, text)

    def readFile(self, file):
        content = ""
        with open(file, "r") as fr:
            for line in fr.readlines():
                content += line
        return content
