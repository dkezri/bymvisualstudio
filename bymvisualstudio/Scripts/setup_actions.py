
import bymvisualstudio
from bymvisualstudio.Actions.Command import Command
from bymvisualstudio.Actions.Query import Query
from bymvisualstudio.Scripts.utils import Utils
import os


class setupActions:

    def __init__(self):
        self.sourceData = "\Data"
        self.utils = Utils()

    def joinDataPath(self, dir, filename):
        currentDir = self.utils.rootDirectoryOfModule(bymvisualstudio)
        path = currentDir + "{}\{}\{}".format(self.sourceData, dir, filename)
        return path

    def joinCurrentRunningPath(self, dir, filename):
        targetDir = self.utils.rootDirectory()
        return os.path.join(targetDir, dir, filename)

    def replaceFileContent(self, dirname, filename, text):
        file = self.joinDataPath(dirname, filename)
        content = self.utils.readFile(file)
        tolower = text.lower()
        toupper = text.title()
        serviceName = self.utils.rootDirectory()
        serviceName = serviceName.split('\\')[-1]
        str = content.replace("ServiceName", serviceName)
        str = str.replace("modelUpper", toupper)
        str = str.replace("modelLower", tolower)

        return str

    def userInputs(self):
        model = input("\n Model/Entity name:")
        model = model.strip()
        if(len(model) == 0):
            print("Model/Entity name is required, please choose a name")
            exit(0)
        return model.lower()

    def createTargetCommandFileName(self, action, entity):
        targetFile = "{}{}Command.cs".format(action.title(), entity.title())
        return targetFile

    def createTargetQueryFileName(self, action, entity):
        targetFile = "{}{}Query.cs".format(action, entity.title())
        return targetFile

    def create(self):
        entity = self.userInputs()
        content = self.replaceFileContent(
            "Commands", "createCommand.cs", entity)
        targetFile = self.createTargetCommandFileName("create", entity)
        targetPath = self.joinCurrentRunningPath("Commands", targetFile)
        self.utils.makeFile(targetPath, content)

    def update(self):
        entity = self.userInputs()
        content = self.replaceFileContent(
            "Commands", "updateCommand.cs", entity)
        targetFile = self.createTargetCommandFileName("update", entity)
        targetPath = self.joinCurrentRunningPath("Commands", targetFile)
        self.utils.makeFile(targetPath, content)

    def delete(self):
        entity = self.userInputs()
        content = self.replaceFileContent(
            "Commands", "deleteCommand.cs", entity)
        targetFile = self.createTargetCommandFileName("delete", entity)
        targetPath = self.joinCurrentRunningPath("Commands", targetFile)
        self.utils.makeFile(targetPath, content)

    def getsingle(self):
        entity = self.userInputs()
        content = self.replaceFileContent(
            "Queries", "GetSingleQuery.cs", entity)
        targetFile = self.createTargetQueryFileName("GetSingle", entity)
        targetPath = self.joinCurrentRunningPath("Queries", targetFile)
        self.utils.makeFile(targetPath, content)

    def getall(self):
        entity = self.userInputs()
        content = self.replaceFileContent("Queries", "GetAllQuery.cs", entity)
        targetFile = self.createTargetQueryFileName("GetAll", entity)
        targetPath = self.joinCurrentRunningPath("Queries", targetFile)
        self.utils.makeFile(targetPath, content)


def create():
    obj = setupActions()
    obj.create()


def update():
    obj = setupActions()
    obj.update()


def delete():
    obj = setupActions()
    obj.delete()


def getsingle():
    obj = setupActions()
    obj.getsingle()


def getall():
    obj = setupActions()
    obj.getall()


if __name__ == "__main__":
    obj = setupActions()
    obj.create()
