using System;
using System.Threading.Tasks;
using BymiljoServiceInfra.Interfaces;
using BymiljoServiceInfra.Queries;
using Microsoft.AspNetCore.Mvc;
using ServiceName.Serviecs;

namespace ServiceName.Queries
{
    public class GetSinglemodelUpperQuery:QueryBase
    {
        [FromRoute]
        public Guid modelUpperId { get; set; }
    }
    public class GetSinglemodelUpperQueryHandler : QueryHandlerBase, IQueryHandler<GetSinglemodelUpperQuery>
    {
        private readonly IValidatorService _validatorService;

        public GetSinglemodelUpperQueryHandler(IValidatorService validatorService)
        {
            _validatorService = validatorService;
        }

        public Task<IQueryResult> Retrieve(GetSinglemodelUpperQuery query)
        {
            var modelLower = _validatorService.ValidatemodelLowerId(query.modelLowerId);
            return Task.FromResult<IQueryResult>(QueryResult.Ok(modelLower));
        }
    }
}
