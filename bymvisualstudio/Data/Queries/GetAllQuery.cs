using System.Threading.Tasks;
using BymiljoServiceInfra.Interfaces;
using BymiljoServiceInfra.Queries;
using ServiceName.Repositories;

namespace ServiceName.Queries
{
    public class GetAllmodelUpperQuery:QueryBase
    {
    }
    public class GetAllmodelUpperQueryHandler:QueryHandlerBase,IQueryHandler<GetAllmodelUpperQuery>
    {
        private readonly ImodelUpperRepository _modelLowerRepository;

        public GetAllmodelUpperQueryHandler(ImodelUpperRepository modelLowerRepository)
        {
            _modelLowerRepository = modelLowerRepository;
        }
        public Task<IQueryResult> Retrieve(GetAllmodelUpperQuery query)
        {
            var modelLowere= _modelLowerRepository.GetAll();
            return Task.FromResult<IQueryResult>(QueryResult.Ok(modelLowere));
        }
    }
}
