using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BymiljoServiceInfra.Commands;
using BymiljoServiceInfra.Interfaces;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using ServiceName.Repositories;
using ServiceName.Serviecs;
using ValidationContext = System.ComponentModel.DataAnnotations.ValidationContext;

namespace ServiceName.Commands
{
    public class UpdatemodelUpperCommand:CommandBase,IValidatableObject
    {
        [FromRoute]
        public Guid modelUpperId { get; set; }
        [FromBody]
        public UpdatemodelUpperCommandBody Body { get; set; }

        internal string Navn => Body.Navn;

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new UpdatemodelUpperCommandValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] {item.PropertyName}));
        }

        private class UpdatemodelUpperCommandValidator:AbstractValidator<UpdatemodelUpperCommand>
        {
            public UpdatemodelUpperCommandValidator()
            {
                RuleFor(prop => prop.Navn).NotEmpty();
            }
        }
    }

    public class UpdatemodelUpperCommandBody
    {
        public string Navn { get; set; }
    }


    public class UpdatemodelUpperCommandHandler : CommandHandlerBase, ICommandHandler<UpdatemodelUpperCommand>
    {
        private readonly ImodelUpperRepository modelLowerRepository;
        private readonly IValidatorService _validatorService;

        public UpdatemodelUpperCommandHandler(ImodelUpperRepository modelLowerRepository, IValidatorService validatorService)
        {
            modelLowerRepository = modelLowerRepository;
            _validatorService = validatorService;
        }

        public Task<ICommandResult> Execute(UpdatemodelUpperCommand command)
        {
            var modelLower = _validatorService.ValidatemodelUpperId(command.modelUpperId);
            _validatorService.CheckIfmodelUpperNameExists(command.Navn);
            modelLower.Navn = command.Navn;
            modelLowerRepository.Commit();
            return Task.FromResult<ICommandResult>(CommandResult.Ok());
        }
    }
}
