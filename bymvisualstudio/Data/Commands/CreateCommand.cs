using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BymiljoServiceInfra.Commands;
using BymiljoServiceInfra.Interfaces;
using FluentValidation;
using ServiceName.Context.Models;
using ServiceName.Repositories;
using ValidationContext = System.ComponentModel.DataAnnotations.ValidationContext;

namespace ServiceName.Commands
{
    public class CreatemodelUpperCommand:CommandBase, IValidatableObject
    {
        public string Navn { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new CreatemodelUpperCommandValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] {item.PropertyName}));
        }

        private class CreatemodelUpperCommandValidator: AbstractValidator<CreatemodelUpperCommand>
        {
            public CreatemodelUpperCommandValidator()
            {
                RuleFor(prop => prop.Navn).NotEmpty();
            }
        }
    }

    public class CreatemodelUpperCommandHandler : CommandHandlerBase, ICommandHandler<CreatemodelUpperCommand>
    {
        private readonly ImodelUpperRepository _modelLowerRepository;

        public CreatemodelUpperCommandHandler(ImodelUpperRepository modelLowerRepository)
        {
            _modelLowerRepository = modelLowerRepository;
        }

        public Task<ICommandResult> Execute(CreatemodelUpperCommand command)
        {
            var modelLower = modelUpper.Create(command);
            _modelLowerRepository.Add(modelLower);
            _modelLowerRepository.Commit();
            return Task.FromResult<ICommandResult>(CommandResult.Ok(new CommandResultGuid(utvikler.Id)));
        }
    }
}
