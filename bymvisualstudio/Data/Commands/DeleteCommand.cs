using System;
using System.Threading.Tasks;
using BymiljoServiceInfra.Commands;
using BymiljoServiceInfra.Interfaces;
using Microsoft.AspNetCore.Mvc;
using ServiceName.Repositories;
using ServiceName.Serviecs;

namespace ServiceName.Commands
{
    public class DeletemodelUpperCommand:CommandBase
    {
        [FromRoute]
        public Guid modelUpperId { get; set; }
    }

    public class DeletemodelUpperCommandHanlder:CommandHandlerBase,ICommandHandler<DeletemodelUpperCommand>
    {
        private readonly ImodelUpperRepository modelLowerRepository;
        private readonly IValidatorService _validatorService;

        public DeletemodelUpperCommandHanlder(ImodelUpperRepository modelLowerRepository, IValidatorService validatorService)
        {
            modelLowerRepository = modelLowerRepository;
            _validatorService = validatorService;
        }

        public Task<ICommandResult> Execute(DeletemodelUpperCommand command)
        {
            var modelLower = _validatorService.ValidateUtviklerId(command.modelUpperId);
            modelLowerRepository.Delete(modelLower);
            modelLowerRepository.Commit();
            return Task.FromResult<ICommandResult>(CommandResult.Ok());
        }

        
    }
}
