cd /d %~dp0

SET latest="latest"
SET currentVersion="1.0.3"
git tag -d %latest%
git push origin :refs/tags/%latest%
git tag %latest%
git push origin %latest%
git tag %currentVersion%
git push origin %currentVersion%