from setuptools import setup, find_packages, Command
import shutil
import os


class CleanUpCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        sourceToDelete = ["bymvisualstudio.egg-info"]
        for f in sourceToDelete:
            if os.path.exists(os.path.join(os.getcwd(), f)):
                shutil.rmtree(f)
                print("removed {}".format(f))


currentVersion = "0.0.1"

setup(
    name="bymvisualstudio",
    version=currentVersion,
    description="A python framework for visualstudio code snippets as helper.",
    author="Oslo Kommune Bymiljøetaten",
    author_email="diako.k@gmail.com",
    license="Bym-developers",
    url="https://bitbucket.org/dkezri/bymvisualstudio",
    download_url="https://bitbucket.org/dkezri/bymvisualstudio/get/{}.tar.gz".format(
        currentVersion),
    keywords=['visualstudio', 'code snippets helper'],
    packages=find_packages(exclude=['tests']),
    package_data={'': ['*.bat', '*.cfg', '*.info']},
    install_requires=['requests==2.12.4', 'yapf==0.14.0', 'grequests',
                      'pydash==4.0.3', 'jsonpickle==0.9.4', 'docker==2.4.2', 'xlrd', 'colored'],
    extras_require={'sys_platform==win32': 'pypiwin32'},
    entry_points={
        'console_scripts': [
            'createcommand=bymvisualstudio.Scripts.setup_actions:create',
            "updatecommand=bymvisualstudio.Scripts.setup_actions:update",
            "deletecommand=bymvisualstudio.Scripts.setup_actions:delete",
            "getsinglequery=bymvisualstudio.Scripts.setup_actions:getsingle",
            "getallquery=bymvisualstudio.Scripts.setup_actions:getall",
        ]
    },
    cmdclass={
        'clean': CleanUpCommand
    }
)
